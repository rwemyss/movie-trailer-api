### Overview
This is a demonstration project to show the use of node.js as a backend to a
REST API. The API accepts a URL of a movie resource as input and returns a
trailer to the movie as an output.

API considerations:

* Code
 * Structure
  * MVC or other pattern
 * Quality
 * Linting
* Documentation (for internal/external use)
* Perhaps API versioning
* Logging
* Caching
* Testing
 * Mock API endpoints to test full range of errors
* Security
 * Rate limiting
 * Setting headers (e.g. helmet.js)
 * OWASP recommendations
 * Check if CORS is required
 * Authentication if needed JWT or API Key
* Environment
 * Target version of node (check engine in package.json)
 * Infrastructure the code will run on (e.g. Lambda)
 * Environmental variables for sensitive data (e.g. API keys)
* Dependencies
 * Does this extra dependencies provide enough use to make up for the cost of an extra dependency?
* Error handling
 * Consuming external services
  * Can you connect to the service?
  * Do you get a response in a timely manner?
  * Does the response contain an error?
  * If the response is good, does it contain the data we expect?
  * Should we retry the service if we don't get the data we expect?
 * Error responses for those consuming this API
  * What error information should be provided? This will likely depend on the audience.
  * Invalid routes

### Installation
Check out:
```
$ git clone https://rwemyss@bitbucket.org/rwemyss/movie-trailer-api.git
```
Install dependencies:
```
$ npm install
```
Set environment variable by creating a .env file in the root of the project,
replace the TMDB API key with a valid one, in the format:
```
TMDB_API_KEY=XXXXXXXXXXXXX
```

### Unit Tests
Run unit tests (this will run the linter pretest also):
```
$ npm test
```

### Running Locally
Setup a local webserver at http://localhost:3000 and run the API:
```
$ npm start
```
Make a request to the API via cURL:
```
$ curl http://localhost:3000/trailer?link=https://content.viaplay.se/pc-se/film/arrival-2016
```
