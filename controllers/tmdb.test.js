const tmdb = require('./tmdb')

test('gets TMDB ID from a IMDB ID', () => {
  return tmdb.getTmdbIdFromTmdbFind('tt2543164').then(data => {
    expect(data).toBe(329865) // TODO check that this should be a number and not a string
  })
})

test('try to get an TMDB id from invalid IMDB id', () => {
  return expect(tmdb.getTmdbIdFromTmdbFind('XYZ'))
    .rejects.toThrow('Error:')
})

test('gets a YouTube URL from a TMDB ID', () => {
  return tmdb.getYouTubeUrlFromTmdbMovie('329865').then(data => {
    expect(data).toBe('https://www.youtube.com/watch?v=gwqSi_ToNPs') // TODO this doesn't match what's in the question brief
  })
})

test('try to get a YouTube URL from invalid TMDB id', () => {
  return expect(tmdb.getYouTubeUrlFromTmdbMovie('XYZ'))
    .rejects.toThrow('Error:')
})
