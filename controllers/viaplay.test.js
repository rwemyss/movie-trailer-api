const viaplay = require('./viaplay')

test('gets IMDB id from a movie resource', () => {
  return viaplay.getImdbIdFromMovieResource('https://content.viaplay.se/pc-se/film/arrival-2016').then(data => {
    expect(data).toBe('tt2543164')
  })
})

test('try to get an invalid movie resource', () => {
  return expect(viaplay.getImdbIdFromMovieResource('https://content.viaplay.se/pc-se/film/xyz'))
    .rejects.toThrow('Error:')
})
