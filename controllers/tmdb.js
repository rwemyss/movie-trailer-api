require('dotenv').load()
const request = require('superagent')

// Get the TMDB id for the movie from the IMDB id provided in the movie resource
const getTmdbId = imdbId => {
  const movieDbUrl = 'https://api.themoviedb.org/3/find/'
  const movieDbApiKey = process.env.TMDB_API_KEY
  const url = movieDbUrl + imdbId + '?api_key=' + movieDbApiKey + '&external_source=imdb_id'

  return request
    .get(url)
    .timeout({
      response: 4000,
      deadline: 8000
    })
    .then(res => {
      if (res.status === 200 && res.body) {
        return res.body
      } else {
        throw new Error('TMDB /find response error occurred')
      }
    })
    .catch(err => {
      throw new Error('TMDB /find connection error:', err)
    })
}

// Parse the JSON from the TMDB find endpoint to get the TMDB for the movie
const parseTmdbIdFromTmdbFind = tmdbFindResource => {
  try {
    return tmdbFindResource.movie_results[0].id
  } catch (err) {
    throw new Error('TMDB ID not available from /find. With error:', err)
  }
}

const getTmdbIdFromTmdbFind = async imdbId => {
  return getTmdbId(imdbId)
    .then(tmdbFindResource => {
      return parseTmdbIdFromTmdbFind(tmdbFindResource)
    })
    .catch(err => {
      throw new Error(err)
    })
}

// Get the trailer url for the movie from the TMDB id provided
const getTrailerUrl = tmdbId => {
  const movieDbUrl = 'https://api.themoviedb.org/3/movie/'
  const movieDbApiKey = process.env.TMDB_API_KEY
  const url = movieDbUrl + tmdbId + '/videos?api_key=' + movieDbApiKey

  return request
    .get(url)
    .timeout({
      response: 4000,
      deadline: 8000
    })
    .then(res => {
      if (res.status === 200 && res.body) {
        return res.body
      } else {
        throw new Error('TMDB /movie response error occurred')
      }
    })
    .catch(err => {
      throw new Error('TMDB /movie connection error:', err)
    })
}

// Parse the JSON from the TMDB movie endpoint and return YouTube URL to trailer
const parseYouTubeUrlFromTmdbMovie = tmdbMovieResource => {
  try {
    return `https://www.youtube.com/watch?v=${tmdbMovieResource.results[0].key}`
  } catch (err) {
    throw new Error('YouTube URL not available from /movie. With error:', err)
  }
}

const getYouTubeUrlFromTmdbMovie = async tmdbId => {
  return getTrailerUrl(tmdbId)
    .then(tmdbMovieResource => {
      return parseYouTubeUrlFromTmdbMovie(tmdbMovieResource)
    })
    .catch(err => {
      throw new Error(err)
    })
}

module.exports.getTmdbIdFromTmdbFind = getTmdbIdFromTmdbFind
module.exports.getYouTubeUrlFromTmdbMovie = getYouTubeUrlFromTmdbMovie
