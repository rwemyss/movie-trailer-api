const request = require('superagent')

// Get the movie resource from the Viaplay endpoint using the movie resource link
const getMovieResource = url => {
  return request
    .get(url)
    .timeout({
      response: 2000,
      deadline: 4000
    })
    .then(res => {
      if (res.status === 200 && res.body) {
        return res.body
      } else {
        throw new Error('Movie resource response error occurred')
      }
    })
    .catch(err => {
      throw new Error('Movie resource connection error:', err)
    })
}

// Parse the JSON from the Viaplay endpoint to get the IMDB for the movie
const parseImdbIdFromMovieResource = movieResource => {
  try {
    return movieResource._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb.id
  } catch (err) {
    throw new Error('IMDB ID not available from movie resource. With error:', err)
  }
}

const getImdbIdFromMovieResource = async url => {
  return getMovieResource(url)
    .then(movieResource => {
      return parseImdbIdFromMovieResource(movieResource)
    })
    .catch(err => {
      throw new Error(err)
    })
}

module.exports.getImdbIdFromMovieResource = getImdbIdFromMovieResource
