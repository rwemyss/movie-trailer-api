const viaplay = require('../controllers/viaplay')
const tmdb = require('../controllers/tmdb')

const router = app => {
  app.get('/trailer', async (req, res) => {
    try {
      // TODO probably want to scrub incoming link
      const imdbId = await viaplay.getImdbIdFromMovieResource(req.query.link)
      const tmdbId = await tmdb.getTmdbIdFromTmdbFind(imdbId)
      const trailerUrl = await tmdb.getYouTubeUrlFromTmdbMovie(tmdbId)
      res.status(200).send(trailerUrl)
    } catch (e) {
      res.status(500).send('Internal error occurred')
    }
  })
}

module.exports.router = router
