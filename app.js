const express = require('express')
const routes = require('./routes/routes')

const app = express()

app.use(express.json())

routes.router(app)

app.listen(3000, () => console.log('Server listening on port 3000!'))
